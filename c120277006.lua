local m=120277006
local list={120277051}
local cm=_G["c"..m]
cm.name="混沌旋涡射手"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--To Hand
	local e1=Effect.CreateEffect(c)
	e1:SetDescription(aux.Stringid(m,0))
	e1:SetCategory(CATEGORY_TOHAND+CATEGORY_GRAVE_ACTION)
	e1:SetType(EFFECT_TYPE_IGNITION)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCost(cm.cost)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.operation)
	c:RegisterEffect(e1)
end
--To Hand
function cm.costfilter(c)
	return c:IsAttribute(ATTRIBUTE_LIGHT+ATTRIBUTE_DARK) and c:IsRace(RACE_GALAXY)
		and c:IsAbleToDeckOrExtraAsCost()
end
function cm.thfilter(c)
	return ((c:IsType(TYPE_NORMAL) and c:IsAttack(1600) and c:IsRace(RACE_GALAXY)) or c:IsCode(list[1])) 
		and c:IsAbleToHand()
end
function cm.costcheck(g,e,tp)
	return Duel.IsExistingMatchingCard(cm.thfilter,tp,LOCATION_GRAVE,0,1,g)
end
function cm.check(g)
	if g:GetCount()<2 then return true end
	local tc1=g:GetFirst()
	local tc2=g:GetNext()
	return (tc1:IsRace(RACE_GALAXY) and tc2:IsCode(list[1]))
		or (tc2:IsRace(RACE_GALAXY) and tc1:IsCode(list[1]))
end
cm.cost=RD.CostSendGraveSubToDeckBottom(cm.costfilter,cm.costcheck,2,2)
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.thfilter,tp,LOCATION_GRAVE,0,1,nil) end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_GRAVE)
end
function cm.operation(e,tp,eg,ep,ev,re,r,rp)
	RD.SelectGroupAndDoAction(HINTMSG_ATOHAND,aux.NecroValleyFilter(cm.thfilter),cm.check,tp,LOCATION_GRAVE,0,1,2,nil,function(g)
		RD.SendToHandAndExists(g,1-tp)
	end)
end