local m=120263001
local list={120263008,120263007}
local cm=_G["c"..m]
cm.name="元素英雄 火焰翼侠"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
	--Only Fusion Summon
	RD.OnlyFusionSummon(c)
	--Damage
	local e1=RD.ContinuousBattleDestroyingDamage(c)
	--Continuous Effect
	RD.AddContinuousEffect(c,e1)
end
--Damage
function cm.damcon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local bc=c:GetBattleTarget()
	return c:IsRelateToBattle() and bc:IsLocation(LOCATION_GRAVE) and bc:IsType(TYPE_MONSTER)
end
function cm.damop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetHandler():GetBattleTarget()
	if tc then
		local dam=RD.GetBaseAttackOnDestroy(tc)
		Duel.Hint(HINT_CARD,0,m)
		Duel.Damage(1-tp,dam,REASON_EFFECT)
	end
end