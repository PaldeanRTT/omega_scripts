local m=120244166
local cm=_G["c"..m]
cm.name="水稻梯田危机"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_POSITION)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_SPSUMMON_SUCCESS)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP)
	e1:SetCondition(cm.condition)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
end
--Activate
function cm.confilter(c,tp)
	return c:GetSummonPlayer()==tp and c:IsSummonLocation(LOCATION_HAND+LOCATION_GRAVE)
end
function cm.filter(c)
	return c:IsFaceup() and c:IsLevelBelow(12) and RD.IsCanChangePosition(c) and c:IsCanTurnSet()
end
function cm.check(g,atk)
	return g:GetSum(Card.GetLevel)<=12
end
function cm.condition(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(cm.confilter,1,nil,1-tp)
end
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.filter,tp,0,LOCATION_MZONE,1,nil) end
	local g=Duel.GetMatchingGroup(cm.filter,tp,0,LOCATION_MZONE,nil)
	Duel.SetOperationInfo(0,CATEGORY_POSITION,g,1,0,0)
end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	RD.SelectGroupAndDoAction(HINTMSG_SET,cm.filter,cm.check,tp,0,LOCATION_MZONE,1,2,nil,function(g)
		RD.ChangePosition(g,POS_FACEDOWN_DEFENSE)
	end)
end