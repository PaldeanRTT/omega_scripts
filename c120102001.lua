local m=120102001
local list={120102002,120145007}
local cm=_G["c"..m]
cm.name="千年龙"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
	RD.SetFusionLegendMaterial(c,{list[1]},{aux.Stringid(m,1)})
end