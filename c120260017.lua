local m=120260017
local cm=_G["c"..m]
cm.name="奇怪吸引子"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DRAW)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCost(cm.cost)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
end
--Activate
function cm.costfilter1(c)
	return c:IsRace(RACE_CYBERSE) and c:IsAbleToGraveAsCost()
end
function cm.costfilter2(c)
	return c:IsLevelAbove(1) and c:IsRace(RACE_CYBERSE) and c:IsAbleToDeckOrExtraAsCost()
end
function cm.costcheck2(g)
	return g:GetClassCount(Card.GetLevel)==g:GetCount()
end
cm.cost=RD.CostChooseAction(HINTMSG_TOGRAVE,HINTMSG_TODECK,aux.Stringid(m,1),
cm.costfilter1,nil,LOCATION_HAND,1,function(g)
	Duel.SendtoGrave(g,REASON_COST)
end,
cm.costfilter2,cm.costcheck2,LOCATION_GRAVE,5,function(g)
	Duel.SendtoDeck(g,nil,SEQ_DECKSHUFFLE,REASON_COST)
end)
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,2) end
	RD.TargetDraw(tp,2)
end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	RD.Draw()
	if Duel.GetFlagEffect(tp,m)~=0 then return end
	RD.CreateCannotActivateEffect(e,aux.Stringid(m,2),cm.aclimit,tp,1,0,RESET_PHASE+PHASE_END)
	Duel.RegisterFlagEffect(tp,m,RESET_PHASE+PHASE_END,0,1)
end
function cm.aclimit(e,re,tp)
	local tc=re:GetHandler()
	return re:IsActiveType(TYPE_MONSTER) and not tc:IsRace(RACE_CYBERSE)
end