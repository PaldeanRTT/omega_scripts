-- Rush Duel 编号
RushDuel = RushDuel or {}

LEGEND_MONSTER = 120000000
LEGEND_SPELL = 120000001
LEGEND_TRAP = 120000002

RushDuel.LegendCodes = {
    -- 青眼白龙
    {120120000, 120198001, 120231001},
    -- 真红眼黑龙
    {120125001, 120203016, 120229101, 120274076},
    -- 黑魔术师
    {120130000, 120203015, 120254001},
    -- 恶魔召唤
    {120145000, 120277000},
    -- 海龙-泰达路斯
    {120199000, 120239060},
    -- 死者苏生
    {120194004, 120195004},
    -- 天使的施舍
    {120196049, 120195005, 120271001}
}

-- 初始化传说卡
function RushDuel.InitLegend()
    local g = Duel.GetMatchingGroup(RushDuel.IsLegendCard, 0, 0xff, 0xff, nil, true)
    g:ForEach(RushDuel.InitLegendCard)
end
function RushDuel.InitLegendCard(c)
    local code = RushDuel.GetLegendCode(c:GetOriginalCode())
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_LEGEND_CARD)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_SET_AVAILABLE)
    e1:SetRange(0xff)
    e1:SetValue(code)
    c:RegisterEffect(e1, true)
    local e2 = e1:Clone()
    e2:SetCode(EFFECT_ADD_LINK_CODE)
    c:RegisterEffect(e2)
    local e3 = e1:Clone()
    e3:SetCode(EFFECT_ADD_FUSION_CODE)
    c:RegisterEffect(e3)
    -- 修改卡牌数据 (删除同名卡：传说卡)
    -- c:SetEntityCode(code, true)
end

-- 获取传说卡原卡名
function RushDuel.GetLegendCode(code)
    for _, codes in ipairs(RushDuel.LegendCodes) do
        for _, legend_code in ipairs(codes) do
            if (code == legend_code) then
                return codes[1]
            end
        end
    end
    return code
end

-- 条件: 是否为传说卡
function RushDuel.IsLegendCard(card, original)
    if original then
        return card:IsOriginalCodeRule(LEGEND_MONSTER, LEGEND_SPELL, LEGEND_TRAP)
    else
        return card:IsHasEffect(EFFECT_LEGEND_CARD) ~= nil
    end
end

-- 获取卡名
function RushDuel.GetCardCode(card)
    local code1, code2 = card:GetLinkCode()
    if code2 then
        return code2
    else
        return code1
    end
end

-- 条件: 是否为传说卡的卡名
function RushDuel.IsLegendCode(card, ...)
    return card:IsLinkCode(table.unpack({...}))
end

-- 条件: 是否为同名卡
function RushDuel.IsSameCode(card1, card2)
    return card1:IsLinkCode(RushDuel.GetCardCode(card2))
end

-- 条件: 是否为原本卡名相同的卡
function RushDuel.IsSameOriginalCode(card1, card2)
    if RushDuel.IsLegendCard(card1, true) and RushDuel.IsLegendCard(card2, true) then
        local code1 = RushDuel.GetLegendCode(card1:GetOriginalCode())
        local code2 = RushDuel.GetLegendCode(card2:GetOriginalCode())
        return code1 == code2
    else
        return card1:IsOriginalCodeRule(card2:GetOriginalCodeRule())
    end
end

-- 永续改变卡名
function RushDuel.EnableChangeCode(c, code, location, condition)
    Auxiliary.AddCodeList(c, code)
    local loc = c:GetOriginalType() & TYPE_MONSTER ~= 0 and LOCATION_MZONE or LOCATION_SZONE
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_CHANGE_CODE)
    e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
    e1:SetRange(location or loc)
    if condition ~= nil then
        e1:SetCondition(condition)
    end
    e1:SetValue(code)
    c:RegisterEffect(e1)
    local e2 = e1:Clone()
    e2:SetCode(EFFECT_ADD_LINK_CODE)
    c:RegisterEffect(e2)
    local e3 = e1:Clone()
    e3:SetCode(EFFECT_ADD_FUSION_CODE)
    c:RegisterEffect(e3)
    return e1, e2, e3
end

-- 当作传说卡 (赝品)
function RushDuel.EnableFakeLegend(card, location)
    local e1 = Effect.CreateEffect(card)
    e1:SetType(EFFECT_TYPE_SINGLE)
    e1:SetCode(EFFECT_LEGEND_CARD)
    e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE + EFFECT_FLAG_UNCOPYABLE + EFFECT_FLAG_SET_AVAILABLE)
    e1:SetRange(location)
    e1:SetValue(0)
    card:RegisterEffect(e1, true)
    return e1
end
